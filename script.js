const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', function(event) {
    const key = event.key.toUpperCase();

    switch (key) {
        case 'S':
        case 'E':
        case 'O':
        case 'N':
        case 'L':
        case 'Z':
        case 'ENTER':
            buttons.forEach(btn => btn.classList.remove('blue-bg'));
            const targetButton = document.getElementById(`myButton-${key.toLowerCase()}`);
            targetButton.classList.add('blue-bg');
            break;
        default:
            break;
    }
});